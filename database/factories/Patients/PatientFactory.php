<?php

namespace Database\Factories\Patients;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Patients\Patient>
 */
class PatientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'external_ID' => Str::upper(fake()->randomLetter()) . fake()->randomNumber(5, true) . Str::upper(fake()->randomLetter() . fake()->randomLetter()) . fake()->randomNumber(4, true),
        ];
    }
}
