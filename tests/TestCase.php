<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;


    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;


    /**
     * Disable vite while running tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutVite();
    }
}
