<?php

namespace Tests\Unit\Patients;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PatientTests extends TestCase
{
    use RefreshDatabase;

    /**
     * This function test that we can access the main landing page.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitLandingPage(): void
    {
        $this->get('/')
            ->assertSuccessful();
    }


    /**
     * This function test that we can access the patients list.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitPatientListingPageAndFindOne(): void
    {
        $this->get(route('patients.index'))
            ->assertSuccessful()
            ->assertSeeText('5');
    }


    /**
     * This function checks the existance of a patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitPatientDetailPage(): void
    {
        $randompatient = fake()->randomNumber(1, true);
        $this->get(route('patients.show', ['patient' => $randompatient]))
            ->assertSuccessful()
            ->assertSeeText($randompatient);
    }


    /**
     * This function checks a non-existing patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitANonExistingPatientDetailPage(): void
    {
        $randompatient = fake()->randomLetter();
        $this->get(route('patients.show', ['patient' => $randompatient]))
            ->assertNotFound();
    }


    /**
     * This function checks the existance of a new patient page.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitNewPatientPage(): void
    {
        $this->get(route('patients.create'))
            ->assertSuccessful();
    }


    /**
     * This function checks the creation of a patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitAndCreateNewPatient(): void
    {
        if ($this->get(route('patients.create'))->assertSuccessful()) {
            $this->post(route('patients.store'), ['external_ID' => 51,])
                ->assertSessionHasNoErrors();
        }
    }


    /**
     * This function checks the non-creation of a patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitAndFailToCreateNewPatient(): void
    {
        if ($this->get(route('patients.create'))->assertSuccessful()) {
            $this->post(route('patients.store'))
                ->assertSessionHasErrors()
                ->assertRedirect();
        }
    }


    /**
     * This function checks access to edit a patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitEditPageForAPatient(): void
    {
        $randompatient = fake()->randomNumber(1, true);
        $this->get(route('patients.edit', ['patient' => $randompatient]))
            ->assertSessionHasNoErrors()
            ->assertSuccessful()
            ->assertViewIs('Patients.edit');
    }


    /**
     * This function checks access to edit a non-existing patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitEditPageForANonExistingPatient(): void
    {
        $randompatient = fake()->randomLetter();
        $this->get(route('patients.edit', ['patient' => $randompatient]))
            ->assertNotFound();
    }


    /**
     * This function checks edits an existing patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitAndUpdateAPatient(): void
    {
        $randompatient = fake()->randomNumber(1, true);
        if ($this->get(route('patients.edit', ['patient' => $randompatient]))->assertSuccessful()) {
            $this->post(route('patients.update', ['patient' => $randompatient]), ['external_ID' => 'MODMODMODMOD',])
                ->assertSessionHasNoErrors();
        }
    }


    /**
     * This function checks the deletion of a patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function visitAndDeleteAnExistingPatient(): void
    {
        $randompatient = fake()->randomNumber(1, true);
        $this->delete(route('patients.destroy', ['patient' => $randompatient]))
            ->assertSessionHasNoErrors()
            ->assertRedirect();
    }


    /**
     * This function checks the deletion of a non-existing patient.
     * @author Juan M. Cortéz <juanm.cortez@gmail.com>
     * @copyright 2023 Juan M. Cortéz
     * @license MIT
     * @version 1.0.05
     *
     * @return void
     *
     * @test
     */
    public function deleteANonExistingPatient(): void
    {
        $randompatient = fake()->randomLetter();
        $this->delete(route('patients.destroy', ['patient' => $randompatient]))
            ->assertNotFound();
    }
}
