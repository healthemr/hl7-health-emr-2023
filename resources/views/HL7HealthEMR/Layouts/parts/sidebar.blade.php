            <label for="hl7HealthEMRMenu" class="drawer-overlay"></label>
            <ul class="menu p-4 w-40 bg-base-100 text-base-content">
                <!-- Sidebar content here -->
                <li>
                    <a href="{{ config('app.url') }}">Dashboard</a>
                </li>
                <li>
                    <a href="{{ route('patients.index') }}">Patients</a>
                </li>
            </ul>
            @section('sidebar')
            @endsection
