                @if (session('success') || session('info') || $errors->any())
                    <div class="toast toast-top toast-end">
                        @if ($errors->any())
                            <div class="flex flex-wrap alert alert-error shadow-lg gap-0 py-2">
                                <div class="flex w-full">
                                    <div class="flex-none w-5">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                            class="stroke-current flex-shrink-0 h-5 w-5 mt-0" fill="none"
                                            viewBox="0 0 24 24">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </div>
                                    <div class="flex-none w-24 font-bold text-xs">ERROR!</div>
                                    <div class="grow">&nbsp;</div>
                                    <div class="flex-none w-24 text-xxs text-right justify-end">
                                        {{ $carbon::parse('now')->format('M d, Y H:i') }}
                                    </div>
                                </div>
                                @foreach ($errors->all() as $error)
                                    <div class="w-full text-sm gap-0 pt-1">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="flex flex-wrap alert alert-success shadow-lg gap-0 py-2">
                                <div class="flex w-full">
                                    <div class="flex-none w-5">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                            class="stroke-current flex-shrink-0 h-5 w-5" fill="none"
                                            viewBox="0 0 24 24">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </div>
                                    <div class="flex-none w-24 font-bold text-xs">SUCCESS</div>
                                    <div class="grow">&nbsp;</div>
                                    <div class="flex-none w-24 text-xxs text-right justify-end">
                                        {{ $carbon::parse('now')->format('M d, Y H:i') }}
                                    </div>
                                </div>
                                <div class="w-full text-sm gap-0 pt-1">{{ session('success') }}</div>
                            </div>
                        @endif
                        @if (session('info'))
                            <div class="flex flex-wrap alert alert-info shadow-lg gap-0 py-2">
                                <div class="flex w-full">
                                    <div class="flex-none w-5">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                            class="stroke-current flex-shrink-0 w-5 h-5">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                        </svg>
                                    </div>
                                    <div class="flex-none w-24 font-bold text-xs">INFO</div>
                                    <div class="grow">&nbsp;</div>
                                    <div class="flex-none w-24 text-xxs text-right justify-end">
                                        {{ $carbon::parse('now')->format('M d, Y H:i') }}
                                    </div>
                                </div>
                                <div class="w-full text-sm gap-0 pt-1">{{ session('info') }}</div>
                            </div>
                        @endif
                    </div>
                @endif
