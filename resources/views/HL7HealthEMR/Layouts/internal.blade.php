@inject('carbon', 'Carbon\Carbon')
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="h-100 w-100" data-theme="{{ config('custom.theme.color') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="application-name" content="{{ config('app.name') }}">
    <meta name="description" content="{{ config('custom.system.description') }}">
    <meta name="keywords" content="{{ config('custom.system.keywords') }}">
    <meta name="author" content="{{ config('custom.system.author') }}">
    <meta name="generator" content="{{ config('custom.system.generator') }}">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">

    <title>@yield('title') | {{ config('app.name') }}</title>

    @vite('resources/js/HL7HealthEMR.js')
</head>

<body>
    <div class="drawer drawer-mobile">
        <input id="hl7HealthEMRMenu" type="checkbox" class="drawer-toggle" />
        <div class="drawer-content flex flex-col items-start justify-start h-full">
            <!-- Navbar -->
            @include('Layouts.parts.navbar')

            <div class="container h-full p-10 mx-auto">
                <!-- Notifications -->
                @include('Layouts.parts.notifications')

                <!-- Page content here -->
                @yield('content')
            </div>

            <!-- Footer -->
            @include('Layouts.parts.footer')
        </div>
        <div class="drawer-side">
            <!-- Sidebar -->
            @include('Layouts.parts.sidebar')
        </div>
    </div>
</body>

</html>
