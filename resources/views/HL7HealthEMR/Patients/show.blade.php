@extends('Layouts.internal')

@section('title', 'Patient Detail')

@section('sidebar')
@endsection

@section('content')
    <table class="table table-zebra w-full">
        <!-- head -->
        <thead>
            <tr class="text-center">
                <th>PID</th>
                <th>EID</th>
                <th>Created</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr class="text-center">
                <td>{{ $patient->pid }}</td>
                <td>{{ $patient->external_ID }}</td>
                <td>{{ $patient->created_at->format('M d, Y H:i') }}</td>
                <td class="flex justify-end">
                    <a class="btn btn-square btn-info mr-2" href="{{ route('patients.edit', ['patient' => $patient->pid]) }}">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-4 h-4">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125" />
                        </svg>

                    </a>
                </td>
            </tr>
        </tbody>
    </table>
    <br />
    <p class="flex justify-end">
        <a href="{{ route('patients.index') }}" class="btn btn-success">Back to list</a>
    </p>
@endsection
