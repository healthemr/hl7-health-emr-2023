@extends('Layouts.internal')

@section('title', 'New Patient')

@section('sidebar')
@endsection

@section('content')
    <div class="grid grid-cols-4 gap-4">
        <form method="POST" action="{{ route('patients.store') }}" class="col-span-3">
            @csrf
            <div class="form-control w-full max-w-xs">
                <label class="label">
                    <span class="label-text">External ID</span>
                </label>
                <div class=" input-group">
                    <input type="text" value="{{ old('external_ID') }}" name="external_ID" placeholder="External ID"
                        class="input input-bordered w-full max-w-xs" />
                    <button type="submit" class="btn">Save</button>
                </div>
            </div>
        </form>
        <p class="flex justify-end">
            <a class="button" href="{{ route('patients.index') }}">Back to list</a>
        </p>
    </div>
@endsection
