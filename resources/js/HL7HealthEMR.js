import './bootstrap';

import "@fortawesome/fontawesome-free/scss/fontawesome.scss";
import "@fortawesome/fontawesome-free/scss/brands.scss";
import "@fortawesome/fontawesome-free/scss/regular.scss";
import "@fortawesome/fontawesome-free/scss/solid.scss";

import '../css/HL7HealthEMR.css';

// static assets for vite reference
// use the following format to reference them
// <img src="{{ Vite::asset('resources/images/logo.png') }}">
import.meta.glob([
    '../images/**',
    '../fonts/**',
]);

import Alpine from 'alpinejs';
window.Alpine = Alpine;
Alpine.start();
