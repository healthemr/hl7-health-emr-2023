<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Patients\PatientController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('Dashboard.index');
});


Route::controller(PatientController::class)
    ->prefix('patients')
    ->name('patients.')
    ->group(function () {
        Route::get('/list/all', 'index')->name('index');
        Route::get('/details/{patient}', 'show')->name('show');
        Route::get('/new', 'create')->name('create');
        Route::post('/new', 'store')->name('store');
        Route::get('/edit/{patient}', 'edit')->name('edit');
        Route::post('/edit/{patient}', 'update')->name('update');
        Route::delete('/destroy/{patient}', 'destroy')->name('destroy');
    });
