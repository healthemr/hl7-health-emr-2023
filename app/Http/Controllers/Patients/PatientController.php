<?php

namespace App\Http\Controllers\Patients;

use App\Models\Patients\Patient;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patients\StorePatientRequest;
use App\Http\Requests\Patients\UpdatePatientRequest;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('Patients.index', ['patients' => Patient::query()->paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('Patients.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePatientRequest $request)
    {
        if ($request->secure()) {
            $patient = Patient::query()->create($request->safe()->all());
            return redirect()
                ->route('patients.show', $patient)
                ->with('success', 'Patient created sucessfully.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Patient $patient)
    {
        return view('Patients.show', ['patient' => $patient]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Patient $patient)
    {
        return view('Patients.edit', ['patient' => $patient]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePatientRequest $request, Patient $patient)
    {
        if ($request->secure()) {
            $patient->update($request->safe()->all());
            return redirect()
                ->route('patients.edit', $patient)
                ->with('success', 'Patient updated sucessfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Patient $patient)
    {
        if ($patient->delete()) {
            return back()->with('success', 'Patient PID ' . $patient->pid . ' removed sucessfully.');
        }
    }
}
