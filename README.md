## HL7 driven Health EMR application

![Status](https://gitlab.com/healthemr/hl7-health-emr-2023/badges/production/pipeline.svg?style=flat-square&key_text=Status) ![Tag](https://img.shields.io/gitlab/v/tag/healthemr/hl7-health-emr-2023?gitlab_url=https%3A%2F%2Fgitlab.com&label=Version&sort=date&style=flat-square) ![Last commit](https://img.shields.io/gitlab/last-commit/healthemr/hl7-health-emr-2023?gitlab_url=https%3A%2F%2Fgitlab.com&label=Last%20commit&style=flat-square) ![Coverage](https://gitlab.com/healthemr/hl7-health-emr-2023/badges/production/coverage.svg?style=flat-square&key_text=Coverage) ![All issues](https://img.shields.io/gitlab/issues/all-raw/healthemr/hl7-health-emr-2023?gitlab_url=https%3A%2F%2Fgitlab.com&label=Issues&style=flat-square) ![License](https://img.shields.io/gitlab/license/healthemr/hl7-health-emr-2023?color=blue&gitlab_url=https%3A%2F%2Fgitlab.com&label=License&style=flat-square) ![Contributors](https://img.shields.io/gitlab/contributors/healthemr/hl7-health-emr-2023?color=blue&gitlab_url=https%3A%2F%2Fgitlab.com&label=Contributors&style=flat-square)


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
