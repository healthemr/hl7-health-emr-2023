<?php

use Illuminate\Support\Facades\Facade;

return [

    /*
    |--------------------------------------------------------------------------
    | Custom config values
    |--------------------------------------------------------------------------
    */

    'theme' => [
        'color'         => env('APP_THEMECOLOR', 'light'),
    ],

    'system' => [
        'description'   => env('APP_DESCRIPTION', ''),
        'keywords'      => env('APP_KEYWORDS', ''),
        'author'        => env('APP_AUTHOR', ''),
        'generator'     => env('APP_GENERATOR', ''),
    ],

    'copyright' => [
        'year'          => env('APP_COPYRIGHTYEAR', date('Y')),
    ],

];
